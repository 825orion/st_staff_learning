public class GraphTester {
    public static void main(String[] args) {
        // Create the verticies
        Vertex A = new Vertex("A");
        Vertex B = new Vertex("B");
        Vertex C = new Vertex("C");
        Vertex D = new Vertex("D");
        Vertex E = new Vertex("E");

        // Link the verticies
        A.addConnection(B);
        B.addConnection(A);
        B.addConnection(C);
        C.addConnection(B);
        C.addConnection(D);
        D.addConnection(C);
        D.addConnection(E);
        E.addConnection(D);

        Vertex[] verticies = { A, B, C, D, E };
        Graph graph = new Graph(verticies);
        System.out.println(graph);
    }
}