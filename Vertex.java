public class Vertex {
    private Vertex[] connections;
    private String name;

    public Vertex(String name) {
        this.name = name;
        this.connections = new Vertex[0];
    }

    public Vertex(String name, Vertex[] connections) {
        this.name = name;
        this.connections = connections;
    }

    public String getName() {
        return this.name;
    }

    public Vertex[] getConnections() {
        return this.connections;
    }

    public void addConnection(Vertex vertex) {
        Vertex[] temp = new Vertex[this.connections.length + 1];
        for (int i = 0; i < this.connections.length; i++) {
            temp[i] = this.connections[i];
        }
        temp[temp.length - 1] = vertex;
        this.connections = temp;
    }

    public void removeConnection(Vertex vertex) {
        Vertex[] temp = new Vertex[this.connections.length - 1];
        boolean offset = false;
        for (int i = 0; i < this.connections.length; i++) {
            if (this.connections[i].getName().equals(vertex.getName())) {
                offset = true;
            }
            if (offset) {
                temp[i] = this.connections[i + 1];
            } else {
                temp[i] = this.connections[i];
            }
        }
        this.connections = temp;
    }
}