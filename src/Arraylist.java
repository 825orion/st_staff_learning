import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
public class Arraylist implements List{
	private Object[] array;
	private int size;
	
	public Arraylist() {
		this.array = new Object[0];
	}
	
	public void print() {
		for(int i=0; i<this.array.length; i++) {
			if(i!=this.array.length-1) {
				System.out.print(this.array[i]+", ");
			}else {
				System.out.println(this.array[i]);
			}
		}
	}
	
	@Override
	public boolean add(Object arg0) {
		Object[] temp = new Object[this.array.length+1];
		for(int i=0; i<this.array.length; i++) {
			temp[i]=this.array[i];
		}
		temp[temp.length-1]=arg0;
		this.array = temp;
		this.size++;
		return true;
	}

	@Override
	public void add(int arg0, Object arg1) {
		Object[] temp = new Object[this.array.length+1];
		for(int i=0; i<temp.length; i++) {
			if(i<arg0) {
				temp[i] = this.array[i];
			}else if(i==arg0) {
				temp[i] = arg1;
			}else {
				temp[i] = this.array[i-1];
			}
		}
		this.size++;
		this.array = temp;
	}

	@Override
	public boolean addAll(Collection arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(int arg0, Collection arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean contains(Object arg0) {
		for(int i=0; i<this.array.length; i++) {
			if(this.array[i] == arg0) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean containsAll(Collection arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object get(int arg0) {
		return this.array[arg0];
	}

	@Override
	public int indexOf(Object arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int lastIndexOf(Object arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ListIterator listIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListIterator listIterator(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Object arg0) {
		for(int i=0; i<this.array.length; i++) {
			if(this.array[i]==arg0) {
				this.remove(i);
				break;
			}
		}
		return false;
	}

	@Override
	public Object remove(int arg0) {
		if(arg0<0 || arg0>this.array.length-1) {
			return null;
		}
		Object[] temp = new Object[this.array.length-1];
		for(int i=0; i<arg0; i++) {
			temp[i]=this.array[i];
		}
		for(int i=arg0+1; i<this.array.length; i++) {
			temp[i-1]=this.array[i];
		}
		this.array = temp;
		this.size--;
		return null;
	}

	@Override
	public boolean removeAll(Collection arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object set(int arg0, Object arg1) {
		Object temp = this.array[arg0];
		this.array[arg0] = arg1;
		return temp;
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public List subList(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray(Object[] arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
