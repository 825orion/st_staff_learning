
public class LinkedListTester {
	public static void main(String[]args) {
		LinkedList list = new LinkedList();
		list.add("Test"); 
		list.add("Test2");
		list.add("Test3");
		list.add(1,"Test1");
		list.remove(0);
		System.out.println(list.indexOf("Test3"));
		list.remove("Test3");
		list.print();
		System.out.println(list.size());
		for(int i=0; i<list.toArray().length; i++) {
			System.out.print(list.toArray()[i] + (i<list.toArray().length-1 ? ", ":""));
		}
	}
}
