import java.util.Scanner;
public class PostFixParser { 
	public static void main(String[] args) {
		Stack stack = new Stack(); 
		//10 4 5 * +
		Scanner scan = new Scanner(System.in);
		String input = scan.nextLine();
		scan.close();
		String[] chars = input.split(" ");
		for(int i=0; i<chars.length; i++) {
			if(chars[i].equals("+")) {
				stack.push(Integer.toString(Integer.parseInt((String)stack.pop())+Integer.parseInt((String)stack.pop())));
			}else if(chars[i].equals("-")) {
				stack.push(Integer.toString(Integer.parseInt((String)stack.pop())-Integer.parseInt((String)stack.pop())));
			}else if(chars[i].equals("*")) {
				stack.push(Integer.toString(Integer.parseInt((String)stack.pop())*Integer.parseInt((String)stack.pop())));
			}else if(chars[i].equals("/")) {
				stack.push(Integer.toString(Integer.parseInt((String)stack.pop())/Integer.parseInt((String)stack.pop())));
			}else {
				stack.push(chars[i]);				
			}
		}
		System.out.println(input+" = "+stack.pop());
	}
}
