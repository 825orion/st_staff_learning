import java.util.ArrayList;

//Get Children: 2n+1, 2n+2
//Get Parent: (n-1)/2
public class Heap {
    private ArrayList<Integer> data;

    public Heap() {
        this.data = new ArrayList<Integer>();
    }

    public Heap(int[] heap) {
        this.data = new ArrayList<Integer>();
        for (int i = 0; i < heap.length; i++) {
            this.data.add(heap[i]);
        }

        for (int i = this.data.size() - 1; i >= 0; i--) {
            downwardHeapify(i);
        }
    }

    public String toString() {
        String temp = "";
        for (int i = 0; i < this.data.size(); i++) {
            temp += this.data.get(i) + (i == this.data.size() - 1 ? "" : ",");
        }
        return temp;
    }

    private boolean isHeap() {
        for (int i = 0; i < this.data.size() - 1; i++) {
            if (!(this.data.get(i) < this.data.get(i + 1))) {
                return false;
            }
        }
        return true;
    }

    public int getHead() {
        return this.data.get(0);
    }

    public void add(int num) {
        this.data.add(num);
        this.upwardHeapify(this.data.size() - 1);
    }

    private void upwardHeapify(int start) {
        if (start != 0) {
            // Do we need to swap?
            if (this.data.get((start - 1) / 2) > this.data.get(start)) {
                // Swap!
                int temp = this.data.get(start);
                this.data.set(start, this.data.get((start - 1) / 2));
                this.data.set((start - 1) / 2, temp);
                // Continue
                this.upwardHeapify((start - 1) / 2);
            }
        }
    }

    private void downwardHeapify(int start) {
        if (start < ((this.data.size() - 1) / 2)) {

            if (this.data.get(start) > this.data.get((2 * start) + 2)
                    || this.data.get(start) > this.data.get((2 * start) + 1)) {
                if (this.data.get((2 * start) + 1) < this.data.get((2 * start) + 2)) {

                    int temp = this.data.get(start);
                    this.data.set(start, this.data.get((start * 2) + 1));
                    this.data.set(((start * 2) + 1), temp);
                    this.downwardHeapify((start * 2) + 1);
                } else {
                    int temp = this.data.get(start);
                    this.data.set(start, this.data.get((start * 2) + 2));
                    this.data.set(((start * 2) + 2), temp);
                    this.downwardHeapify((start * 2) + 2);
                }
            }
        }
    }

    public int removeHead() {
        int head = this.data.get(0);
        int foot = this.data.get(this.data.size() - 1);
        this.data.remove(this.data.size() - 1);
        if (this.data.size() != 0) {
            this.data.set(0, foot);
            this.downwardHeapify(0);
        }
        return head;
    }

    public void changeValue(int index, int newValue) {
        this.data.set(index, newValue);
        this.upwardHeapify(index);
        this.downwardHeapify(index);
    }

    public void remove(int index) {
        this.changeValue(index, Integer.MIN_VALUE);
        this.upwardHeapify(index);
        this.removeHead();
    }
}