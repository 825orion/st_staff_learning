
public class BinarySearchTreeTester {
	public static void main(String[] args) {
		BinarySearchTree tree = new BinarySearchTree(new BinaryNode(10));
		tree.add(9);
		tree.add(11);
		tree.add(5);
		tree.add(15);
		tree.add(19);
		tree.add(-1);
		tree.add(100);
		tree.add(200);
		tree.add(-100);
		tree.add(-1000);
		tree.printFormat();
		System.out.println(tree.findParent(new BinaryNode(-1)).getData());
	}

}
