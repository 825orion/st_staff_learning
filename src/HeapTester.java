public class HeapTester {
    public static void main(String[] args) {
        int[] list = new int[7];
        for (int i = 6; i >= 0; i--) {
            list[list.length - 1 - i] = i;
        }

        Heap heap = new Heap(list);
        System.out.println(heap);
        heap.removeHead();
        System.out.println(heap);

        int[] list2 = { 2, 500, 3, 574, 501, 7 };
        heapSort(list2);
        for (int i = 0; i < list2.length; i++) {
            System.out.print(list2[i] + (i == list2.length - 1 ? "\n" : ", "));
        }
    }

    public static void heapSort(int[] list) {
        Heap heap = new Heap(list);
        for (int i = 0; i < list.length; i++) {
            list[i] = heap.removeHead();
        }
    }
}