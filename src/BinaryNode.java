public class BinaryNode {
	private BinaryNode left;
	private BinaryNode right;
	private int data;
	
	public BinaryNode(int d) {
		this.data = d;
		this.left = null;
		this.right = null;
	}
	
	public BinaryNode() {
		this.data = 0;
		this.left = null;
		this.right = null;
	}
	
	public void setLeft(BinaryNode n) {
		this.left = n;
	}
	
	public void setRight(BinaryNode n) {
		this.right = n;
	}
	
	public void setData(int d) {
		this.data = d;
	}
	
	public BinaryNode getLeft() {
		return this.left;
	}
	
	public BinaryNode getRight() {
		return this.right;
	}
	
	public int getData() {
		return this.data;
	}
	
}
