public class HashmapTester {
    public static void main(String[] args) {
        Hashmap map = new Hashmap();
        map.put("Hello, World!", "Hi there!");
        System.out.println(map.get("Hello, World!"));
        map.remove("Hello, World!");
    }
}