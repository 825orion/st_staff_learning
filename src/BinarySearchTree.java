/**
* A Binary Search Tree that contains Binary Nodes in the format:
* <p>
* Left = Less, Right = Greater
* 
* @author Brin Brody
* 
*/
public class BinarySearchTree {
	/**
	 * The head BinaryNode: The first node in the Binary Search Tree
	 */
	private BinaryNode head;
	
	/**
	 * The number of BinaryNodes in the Binary Search Tree
	 */
	private int size;

	/**
	 * Constructs a new (empty) Binary Search Tree where empty is defined as...
	 * <p>
	 * No head node, and 0 size
	 */
	public BinarySearchTree() {
		this.head = null; //Set the head to empty
		this.size = 0; //This tree has no nodes
	}

	/**
	 * Constructs a new Binary Search tree, not empty
	 * @param head - the first BinaryNode in the tree (of which all other nodes will be children)
	 */
	public BinarySearchTree(BinaryNode head) {
		this.head = head; //Set the head to the passed node
		this.size = 1; //This tree now has one node
	}

	/**
	 * Gets the BinaryNode at the head of this Binary Search Tree
	 * @return first BinaryNode in the tree
	 */
	public BinaryNode getHead() {
		return this.head; //get the head node
	}

	/**
	 * Updates the BinaryNode at the head of this Binary Search Tree to a new BinaryNode
	 * @param head - the BinaryNode to become the head node
	 */
	public void setHead(BinaryNode head) {
		this.head = head; //change the head node
	}

	/**
	 * Gets the number of nodes in the Binary Search Tree
	 * @return the number of nodes in the tree
	 */
	public int size() {
		return this.size; //get the size of the tree
	}

	/**
	 * Wrapper of add to allow user to call without passing a check node
	 * @param o - the data for the node to be added
	 * @return success or failure of adding to the tree
	 */
	public boolean add(int o) {
		return this.add(this.head,o); //call the main function starting on the head node
	}

	/**
	 * Adds the passed data as a new node in the right position on the tree
	 * @param check - the current node (in the progression); should start with the head node in most cases
	 * @param o - the data for the node to be added
	 * @return success or failure of adding to the tree
	 */
	public boolean add(BinaryNode check, int o) {
		if(o<check.getData()) { //If the value goes to the left of the current node...
			if(check.getLeft()==null) { //Is the current node the farthest left?
				check.setLeft(new BinaryNode(o)); //Add the new node to the left
				this.size++; //Increase the size of the tree
				return true; //Successful add!
			}else { //If there's still room to go down...
				this.add(check.getLeft(),o); //Go to the left and try it again
			}
		}
		if(o>=check.getData()) { //If the value goes to the right of the current node...
			if(check.getRight()==null) { //Is the current node the farthest right?
				check.setRight(new BinaryNode(o)); //Add the new node to the right
				this.size++; //Increase the size of the tree
				return true; //Successful add!
			}else { //If there's still room to go down...
				this.add(check.getRight(),o); //Go to the right and try it again
			}

		}

		return false; //If we've reached this point, the add has failed
	}

	/**
	 * Wrapper to find the parent of a given BinaryNode in the tree
	 * @param n - the node whose parent is searched for
	 * @return the parent node of n
	 */
	public BinaryNode findParent(BinaryNode n) {
		return this.findParent(n,this.head); //Call the main function starting on the head node
	}

	/**
	 * Finds the parent of a given BinaryNode in the tree
	 * @param n - the node whose parent is searched for
	 * @param current - the current node in the progression (should start at the head, in most cases)
	 * @return the parent node of n, or null if not found
	 */
	public BinaryNode findParent(BinaryNode n, BinaryNode current) {
		if(n==null) { //If this node does not exist
			return null; //This node does not exist (and so it has no parent)
		}

		else if(current.getLeft()==null && current.getRight()==null) {//If this is the last node in the tree and not found...
			return null; //This node does not exist (and so it has no parent)
		}
		else { //If both sides exist
			if(current.getLeft()!=null) { //If the left side exists...
				if(current.getLeft().getData()==n.getData()) { //If the left side equals the searched for node...
					return current; //This is the parent
				}
			}
			if(current.getRight()!=null) { //If the right side exists...
				if(current.getRight().getData()==n.getData()) { //If the right side equals the searched for node...
					return current; //This is the parent
				}
			}

			if(current.getData()<n.getData()) { //If this node is less than the searched for node...
				return this.findParent(n, current.getRight()); //Search to the right for the node and return the found value
			}else if(current.getData()>=n.getData()) { //If this node is greater than (or equal to) the searched for node...
				return this.findParent(n, current.getLeft()); //Search to the left of the node and return the found value
			}else { //When all else fails...
				return null; //There is no parent
			}
		}
	}

	/**
	 * Wrapper to remove a given node from the tree
	 * @param n - the node to remove from the tree
	 * @return success or failure of the removal
	 */
	public boolean remove(BinaryNode n) {
		return remove(n,this.head); //Call the main code starting on the head node
	}

	/**
	 * Function to remove a given node from the tree
	 * @param n - the node to remove from the tree
	 * @param current - the current node in the progression
	 * @return success or failure of the removal
	 */
	public boolean remove(BinaryNode n, BinaryNode current) {
		if(n==null) { //If the node does not exist...
			return false; //You can not remove it
		}else if(current.getLeft()==null && current.getRight()==null) { //If this is the last node and it has not yet been removed...
			return false; //The node does not exist, and you can not remove it
		}else { //If the node can exist so far...
			if(current.getData()==n.getData()) { //If this is the node...
				BinaryNode parent = this.findParent(current); //Find this node's parent
				boolean direction = true; //true = left, false = right
				if(parent.getRight()==current) { //If the node is on the parent's right side...
					direction = false; //set the direction to right
				}
				if(current.getLeft()==null && current.getRight()==null) { //If there are no nodes under this one on this branch...
					if(direction) { //If this node is on the parent's left side...
						parent.setLeft(null); //Wipe the parent's left side
					}else { //If this node is on the parent's right side...
						parent.setRight(null); //Wipe the parent's right side
					}
				}else if(current.getLeft()==null && current.getRight()!=null){ //If there are only nodes greater than (right of) this one...
					if(direction) { //If this node is on the parent's left side...
						parent.setLeft(current.getRight()); //Replace the parent's right side with this node's child
					}else { //If this node is on the parent's right side...
						parent.setRight(current.getRight()); //Replace the parent's left side with this node's child
					}
				}else if(current.getLeft()!=null && current.getRight()==null){ //If there are only nodes less than (left of) this one...
					if(direction) { //If this node is on the parent's left side...
						parent.setLeft(current.getLeft()); //Replace the parent's right side with this node's child
					}else { //If this node is on the parent's right side...
						parent.setRight(current.getLeft()); //Replace the parent's left side with this node's child
					}
				}else { //If there are nodes on both sides of this one... 	
					//Do... something
				}
				return true; //Removal successful!
			}else { //If this is not the node...
				if(current.getData()<n.getData()) { //If this node is less than the searched for node...
					return this.remove(n, current.getRight()); //Search to the right for the node and remove when you find it
				}else if(current.getData()>=n.getData()) { //If this node is greater than (or equal to) the searched for node...
					return this.remove(n, current.getLeft()); //Search to the left of the node and remove when you find it
				}
			}
			return true; //Removal successful!
		}
	}

	/**
	 * Wrapper to print this Binary Search Tree
	 * @return success or failure of the printing
	 */
	public boolean print() {
		return this.print(this.head); //Run the main function to print starting at the head
	}
	/**
	 * Function to print this Binary Search Tree
	 * @param node - the current node in the progression (should usually start at the head node)
	 * @return success or failure of the printing
	 */
	public boolean print(BinaryNode node) {
		if(node==null) { //If they pass no node, there's nowhere for us to go...
			return false; //Print fails
		}
		System.out.println(node.getData()); //Print the current data
		if(node.getLeft()!=null) { //If you can go left...
			this.print(node.getLeft()); //Go to the left and do it again
		}if(node.getRight()!=null) { //If you can go right...
			this.print(node.getRight()); //Go to the right and do it again
		}
		return true; //Print success!
	}

	/**
	 * Wrapper to print the tree formatted
	 * @return success or failure of the printing
	 */
	public boolean printFormat() {
		return this.printFormat(this.head,0); //Run the main function to print starting at the head
	}

	/**
	 * Function to print the tree formatted
	 * @param node - the current node in the progression
	 * @param depth - the depth (distance from head) of the current node
	 * @return success or failure of the printing
	 */
	public boolean printFormat(BinaryNode node, int depth) {
		if(node==null) { //If no node is passed, there is no tree...
			return false; //Print fails
		}
		if(node.getLeft()!=null) { //If you can go left...
			this.printFormat(node.getLeft(),depth+1); //Go left, and do it again with an additional depth
		}
		String temp = ""; //Will store spaces here
		for(int i=0; i<depth*3; i++) {temp+=" ";} //Add 3*depth number of spaces
		System.out.println(temp + node.getData()); //Print the data with the correct number of spaces before
		if(node.getRight()!=null) { //If you can go right...
			this.printFormat(node.getRight(),depth+1); //Go right, and do it again with an additional depth
		}
		return true; //Print success!
	}

}