import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class LinkedList implements List{
	private int size;
	private Node first;
	
	public LinkedList() {
		this.size = 0;
		this.first = null;
	}
	
	@Override
	public boolean add(Object e) {
		if(this.size==0) { //Does it have any nodes?
			this.first = new Node(e); //Make one, and set it to the first one
			this.size++; //Add one node to the size
			return true;
		}
		Node temp = this.first; //Start at the top of the list
		while(temp.getNext()!=null) { //Until the end of the list...
			temp = temp.getNext(); //Select the next node
		}
		temp.setNext(new Node(e)); //Set the last node's next node equal to a new node with the right data
		this.size++; //Add one node to the size
		return true;
	}

	@Override
	public void add(int index, Object element) {
		int count = 0; //Start at the 1st index (0)
		Node temp = this.first; //Start at the first Node
		while(count!=index-1) { //Until you reach one before the index...
			temp = temp.getNext(); //Move to the next node
			count++; //Increment the index
		}
		Node temp2 = temp.getNext(); //Store the previous next node
		Node newNode = new Node(element); //Make the new node
		temp.setNext(newNode); //Insert the node into the list
		newNode.setNext(temp2); //Set the nodes back into place
	}

	@Override
	public boolean addAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(int index, Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean contains(Object o) {
		return this.indexOf(o)!=-1;
	}

	@Override
	public boolean containsAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object get(int index) {
		int count = 0; //Start at the 1st index (0)
		Node temp = this.first; //Start at the first Node
		while(count!=index) { //Until you reach the index...
			temp = temp.getNext(); //Move to the next node
			count++; //Increment the index
		}
		return temp.getData(); //Return the data at the index
	}

	@Override
	public int indexOf(Object o) {
		int count = 0; //Start at the 1st index (0)
		Node temp = this.first; //Start at the first Node
		while(temp.getData()!=o) { //Until you reach the right data...
			temp = temp.getNext(); //Move to the next node
			count++; //Increment the index
			if(count>=this.size) { return -1; } //In case the data is never found
		}
		return count; //Return the index
	}

	@Override
	public boolean isEmpty() {
		return this.first == null;
	}

	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int lastIndexOf(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ListIterator listIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListIterator listIterator(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Object o) {
		this.remove(this.indexOf(o));
		return true;
	}

	@Override
	public Object remove(int index) {
		if(index==0) { //If they want to remove the first node...
			Object tempData = this.first.getData(); //Store the data from the node
			this.first = this.first.getNext(); //Remove the first node
			return tempData; //Return the data from that node
		}
		int count = 0; //Start at the 1st index (0)
		Node temp = this.first; //Start at the first Node
		while(count!=index-1) { //Until you reach one before the right data...
			temp = temp.getNext(); //Move to the next node
			count++; //Increment the index
		}
		Object tempData = temp.getNext().getData(); // 
		temp.setNext(temp.getNext().getNext());
		this.size--;
		return tempData; //Return the index
	}

	@Override
	public boolean removeAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object set(int index, Object element) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public List subList(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray() {
		Object[] array = new Object[this.size-1];
		Node temp = this.first; //Start at the top of the list
		int count = 0; //Start at the first index (0)
		while(temp.getNext()!=null) { //Until the end of the list...
			array[count] = temp.getData(); //Store the data in the array
			temp = temp.getNext(); //Select the next node
			count++; //Go to the next index
		}
		array[count] = temp.getData(); //Store the last index
		return array;
		
	}

	@Override
	public Object[] toArray(Object[] a) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void print() {
		Node temp = this.first; //Start at the top of the list
		while(temp.getNext()!=null) { //Until the end of the list...
			System.out.print(temp.getData()+", "); //Print each index followed by a comma
			temp = temp.getNext(); //Select the next node
		}
		System.out.println(temp.getData()); //Print final index with a new line
	}

}
