
public class Queue {
	private Node head;
	private int size;
	
	public Queue() { //Make an empty Queue
		this.head = null; //No head Node
		this.size = 0; //No elements in the queue
	}
	
	public boolean add(Object o) { //Add an object to the top
		this.size++; //Increase the size
		if(this.head==null) { this.head = new Node(o); return true;} //If queue empty, make this the head
		Node temp = this.head; //Start at the head node
		while(temp.getNext()!=null) { //Repeat until the highest node
			temp = temp.getNext(); //Switch to the next node
		}
		temp.setNext(new Node(o)); //Set the new node as the highest node
		return true; //Success!
	}
	
	public Object remove() { //Remove the lowest element from the queue
		Node temp = this.head; //Store the head node
		this.head = temp.getNext(); //Set the new head to the second node
		this.size--; //Reduce the size
		return temp.getData(); //Return the removed data
	}
	
	/*//TESTING PURPOSES ONLY
	public void print() {
		if(this.size==0) {
			System.out.println("");
		}else {
			Node temp = this.head; //Start at the top of the list
			while(temp.getNext()!=null) { //Until the end of the list...
				System.out.print(temp.getData()+", "); //Print each index followed by a comma
				temp = temp.getNext(); //Select the next node
			}
			System.out.println(temp.getData()); //Print final index with a new line
		}
	}
	*/
}
