
public class Stack {
	
	private Node top;
	private int size;
	
	public Stack() { //Make an empty stack
		this.top = null; //No top node yet
		this.size = 0; //And so no size yet
	}
	
	public boolean empty() { //Is it empty?
		return this.size==0; //If size is 0, true, else, false
	}
	
	public Object peek() { //Peek at the top of the stack
		return this.top;
	}
	
	public Object pop() { //Remove the top of the stack
		Node temp = this.top; //Store the top value
		this.top = top.getNext(); //Move it down the stack
		return temp.getData(); //Return the top value
	}
	
	public Object push(Object o) { //Add to the top of the stack
		Node temp = new Node(o); //Make a new node with the object as data
		temp.setNext(this.top); //Put the current top under the new top
		this.top = temp; //Put the new top into place
		return o; //Return the data
	}
	
	/*
	public int search(Object o) { //Find the first index of the object
		Node temp = this.first; //Start on the first node
		int count = 0; //Start at the first (0th) index
		while(temp.getData()!=o) { //Repeat until the correct index
			temp = temp.getNext(); //Switch to the next node
			count++; //Increment the counter
		}
		return count; //Return the first index of the object
	}
	*/
	
	/*//TESTING PURPOSES ONLY
	public void print() {
		if(this.size==0) {
			System.out.println("");
		}else {
			Node temp = this.top; //Start at the top of the list
			while(temp.getNext()!=null) { //Until the end of the list...
				System.out.print(temp.getData()+", "); //Print each index followed by a comma
				temp = temp.getNext(); //Select the next node
			}
			System.out.println(temp.getData()); //Print final index with a new line
		}
	}
	*/
}
