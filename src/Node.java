public class Node {
	private Node next;
	private Object data;
	
	public Node(Object d) {
		this.data = d;
	}
	
	public Node() {
		this.data = null;
	}
	
	public void setNext(Node n) {
		this.next = n;
	}
	
	public void setData(Object d) {
		this.data = d;
	}
	
	public Node getNext() {
		return this.next;
	}
	
	public Object getData() {
		return this.data;
	}
	
}
