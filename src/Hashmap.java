import java.util.LinkedList;

public class Hashmap {
    private LinkedList<Pair>[] lists;
    private int size;
    public final int initialBuckets = 4;

    public Hashmap() {
        this.lists = new LinkedList[initialBuckets];
        for (int i = 0; i < this.lists.length; i++) {
            this.lists[i] = new LinkedList<Pair>();
        }
        this.size = 0;
    }

    private int hash(String input) {
        int cur = input.charAt(0);
        for (int i = 0; i < input.length(); i++) {
            if (i % 3 == 0) {
                cur = cur * input.charAt(i);
            } else if (i % 2 == 0) {
                cur = cur - input.charAt(i);
            } else {
                cur = cur + input.charAt(i);
            }
        }
        return cur;
    }

    public boolean put(String key, String value) {
        Pair newPair = new Pair(key, value);
        int hashedKey = this.hash(newPair.getKey());
        int location = hashedKey % this.lists.length;
        for (int i = 0; i < this.lists[location].size(); i++) {
            if (this.lists[location].get(i).getKey().equals(newPair.getKey())) {
                return false;
            }
        }
        this.lists[location].add(newPair);
        this.size++;
        if (this.size > this.lists.length / 2) {
            this.resize();
        }
        return true;
    }

    public String get(String key) {
        int hashedKey = this.hash(key);
        int location = hashedKey % this.lists.length;
        for (Pair i : this.lists[location]) {
            if (i.getKey().equals(key)) {
                return i.getValue();
            }
        }
        return null;
    }

    public String remove(String key) {
        int hashedKey = this.hash(key);
        int location = hashedKey % this.lists.length;
        for (int i = 0; i < this.lists[location].size(); i++) {
            if (this.lists[location].get(i).getKey().equals(key)) {
                String value = this.lists[location].get(i).getValue();
                this.lists[location].remove(i);
                this.size--;
                return value;
            }
        }
        return null;
    }

    private void resize() {
        LinkedList[] temp = new LinkedList[this.lists.length * 2];
        for (int i = 0; i < this.lists.length; i++) {
            for (int j = 0; j < this.lists[i].size(); j++) {
                String key = this.lists[i].get(j).getKey();
                String value = this.lists[i].get(j).getValue();
                Pair newPair = new Pair(key, value);
                int hashedKey = this.hash(newPair.getKey());
                int location = hashedKey % this.lists.length;
                for (int k = 0; k < this.lists[location].size(); i++) {
                    if (!this.lists[location].get(i).getKey().equals(newPair.getKey())) {
                        this.lists[location].add(newPair);
                    }
                }
            }
        }
        this.lists = temp;
    }
}