public class Graph {
    private Vertex[] verticies;

    public Graph(Vertex[] verticies) {
        this.verticies = verticies;
    }

    public String toString() {
        String str = "";
        str += ("   ");
        for (int i = 0; i < this.verticies.length; i++) {
            str += (this.verticies[i].getName() + " ");
        }
        // Go to the first row
        str = "\n";
        for (int i = 0; i < this.verticies.length; i++) { // For each vertex...
            str += (this.verticies[i].getName() + ": "); // Name the row
            for (int j = 0; j < this.verticies.length; j++) { // For each vertex..
                // Check if they're connected
                boolean connected = false;
                // For each connection in this Vertex...
                for (int k = 0; k < this.verticies[i].getConnections().length; k++) {
                    if (this.verticies[i].getConnections()[k].getName().equals(this.verticies[j].getName())) {
                        // If the name of the current Vertex matches that of the current connection...
                        // They're connected! Add a 1 for this
                        connected = true;
                    } else {
                        // They're not connected! Add a 0 for this
                        connected = false;
                    }
                }
                str += (connected ? "1 " : "0 ");
            }
            // Go to the next row
            str += "\n";
        }
        return str;
    }

    public void addVertex(Vertex vertex) {
        Vertex[] temp = new Vertex[this.verticies.length + 1];
        for (int i = 0; i < this.verticies.length; i++) {
            temp[i] = this.verticies[i];
        }
        temp[temp.length - 1] = vertex;
        this.verticies = temp;
    }

    public void removeVertex(Vertex vertex) {
        Vertex[] temp = new Vertex[this.verticies.length - 1];
        boolean offset = false;
        for (int i = 0; i < this.verticies.length; i++) {
            if (this.verticies[i].getName().equals(vertex.getName())) {
                offset = true;
            }
            if (offset) {
                temp[i] = this.verticies[i + 1];
            } else {
                temp[i] = this.verticies[i];
            }

        }
        this.verticies = temp;
    }

    public Vertex getVertex(int index) {
        return this.verticies[index];
    }
}